# Rendu "Injection"

## Binome

Nom, Prénom, email: Roy Victor victor.roy.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 
    Il nous force a n'utiliser que des chiffres et des lettres

* Est-il efficace? Pourquoi? 
Oui mais il limite fortement l'utilisateur

## Question 2

* Votre commande curl

curl 'http://localhost:8080/' -X POST -H 'Content-Type: application/x-www-form-urlencoded' --data-raw 'chaine=chaîne illégal *$ù &submit=OK'


## Question 3

* changez le who : 

curl 'http://localhost:8080/' -X POST -H 'Content-Type: application/x-www-form-urlencoded' --data-raw "chaine=who injecté','200.200.200.200')#&submit=OK"

* Expliquez comment obtenir des informations sur une autre table

il suffit d'injecter le SQL permettant de récupérer les donées (select * from xxx) et de les envoyer vers un autre lien html toujours dans l'injection.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

* Commande curl pour lire les cookies

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.


